package solitaire.view.game;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import solitaire.model.*;

import java.util.ArrayList;
import java.util.List;


public class GameView extends BorderPane {
    private GridPane gridPaneSpel;
    private GridPane gridPaneDeck;
    private List<ImageView> kaartFoto;
    private ImageView[] uitspelenImgView;
    private Rectangle[] uitspelenRec;
    private final double kolomBreedte;
    private final double kaartBreedte;
    private final double kaartHoogte;
    private Deck deck;
    private Rectangle[] stapels;
    private static final double GRIDPANE_BREEDTE = 1380.0;

    private int speelKaartnr = 0;
    private List<ImageView> deckImages;
    private Label lblBottom;
    private Button btnDeck;
    private ImageView imgDeck;


    private List<GridPane> gridpaneList;




    private MenuItem newGame;
    private MenuItem highScore;
    private MenuItem exit;
    private MenuItem gameRules;
    private MenuItem about;

    public GameView() {
        this.kolomBreedte = GRIDPANE_BREEDTE / GameModel.COLUMS;
        this.kaartBreedte = 143;
        this.kaartHoogte = 219;

        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        this.gridpaneList = new ArrayList<>(7);
        this.deckImages = new ArrayList<>(52);
        this.gridPaneSpel = new GridPane();
        this.gridPaneDeck = new GridPane();
        this.kaartFoto = new ArrayList<>();
        this.lblBottom = new Label("Tijd: " + "Score: ");
        this.imgDeck = new ImageView("images/gray_back.png");
        this.btnDeck = new Button();
        this.newGame = new MenuItem("New Game");
        this.highScore = new MenuItem("High Score");
        this.exit = new MenuItem("Exit");
        this.gameRules = new MenuItem("Game Rules");
        this.about = new MenuItem("About");
        this.uitspelenImgView = new ImageView[4];
        this.uitspelenRec = new Rectangle[4];
        this.deck = new Deck();
        this.stapels = new Rectangle[7];
;


    }

    private void layoutNodes() {
        //image
        for (int i = 0; i < 21; i++) {
            deckImages.add(new ImageView(new Image("images/gray_back.png")));
        }

        // HET MENU OP DE BORDERPANE
        final Menu gameMenu = new Menu("Game");
        gameMenu.getItems().add(this.newGame);
        gameMenu.getItems().add(this.highScore);
        gameMenu.getItems().add(this.exit);
        final Menu helpMenu = new Menu("Help");
        helpMenu.getItems().add(this.gameRules);
        helpMenu.getItems().add(this.about);
        MenuBar menuBar = new MenuBar(gameMenu, helpMenu);


        //LABEL BOTTOM BORDERPANE
        lblBottom.setId("lblBottom");

        // ATTRIBUTEN OP BORDERPANE
        this.setTop(menuBar);
        this.setCenter(this.gridPaneSpel);
        this.setBottom(this.lblBottom);
        BorderPane.setAlignment(lblBottom, Pos.BOTTOM_CENTER);


        ColumnConstraints[] kolommen = new ColumnConstraints[7];
        for (int i = 0; i < 7; i++) {
            kolommen[i] = new ColumnConstraints(kolomBreedte);
            this.gridPaneSpel.getColumnConstraints().add(kolommen[i]);
        }

        this.gridPaneSpel.add(gridPaneDeck, 1, 0);

        //GRIDPANE INDELING (kolommen en rijen)
        RowConstraints row1 = new RowConstraints();
        row1.setPercentHeight(30);
        RowConstraints row2 = new RowConstraints();
        row2.setPercentHeight(70);
        this.gridPaneSpel.getRowConstraints().addAll(row1, row2);
        gridPaneSpel.setId("gridpaneGame");

        for (int i = 0; i < 7; i++) {
            gridpaneList.add(new GridPane());
            gridPaneSpel.add(gridpaneList.get(i), i, 1);
            gridpaneList.get(i).setMaxWidth(kaartBreedte);
            gridPaneSpel.setMargin(gridpaneList.get(i), new Insets(0, 0, 0, 18));
        }

        //IMAGEDECK
        imgDeck.setFitWidth(kaartBreedte);
        imgDeck.setFitHeight(kaartHoogte);

        //DECK BUTTON OP GRIDPANE
        gridPaneSpel.add(btnDeck, 0, 0);
        gridPaneSpel.setMargin(btnDeck, new Insets(10));
        btnDeck.setPrefSize(kaartBreedte, kaartHoogte);
        btnDeck.setGraphic(imgDeck);

        btnDeck.setId("btnDeck");

        //UITSPEELSTAPELS OP GRIDPANE
        for (int i = 0; i < 4; i++) {
            uitspelenRec[i] = new Rectangle(kaartBreedte, kaartHoogte, Color.DARKGREEN);
            gridPaneSpel.setMargin(uitspelenRec[i], new Insets(10));
            gridPaneSpel.add(uitspelenRec[i], i + 3, 0);
        }
        for (int i = 0; i < 4; i++) {
            uitspelenImgView[i] = new ImageView(new Image("images/legeKaart.png"));
            uitspelenImgView[i].setFitHeight(kaartHoogte);
            uitspelenImgView[i].setFitWidth(kaartBreedte);
            //gridPaneSpel.setMargin(uitspelen[i], new Insets(10));
            gridPaneSpel.setHalignment(uitspelenImgView[i], HPos.CENTER);
            gridPaneSpel.add(uitspelenImgView[i], i + 3, 0);
        }

        // 7 RECTANGLE SPEELSTAPELS
        for (int i = 0; i < 7; i++) {
            stapels[i] = new Rectangle(kaartBreedte, kaartHoogte);
            //stapels[i].setVisible(false);

            gridpaneList.get(i).add(stapels[i], i, 1);
            gridpaneList.get(i).setHalignment(stapels[i], HPos.CENTER);
            gridpaneList.get(i).setValignment(stapels[i], VPos.TOP);
        }

        int teller = 0;
        //achterkant kaart
        for (int i = 1; i < 7; i++) {
            for (int j = 0; j < i; j++) {
                gridpaneList.get(i).add(deckImages.get(teller), i, 1);
                gridPaneSpel.setAlignment(Pos.TOP_CENTER);
                gridpaneList.get(i).setHalignment(deckImages.get(teller), HPos.CENTER);
                gridpaneList.get(i).setValignment(deckImages.get(teller), VPos.TOP);
                gridpaneList.get(i).setMargin(deckImages.get(teller), new Insets(j * 30, 0, 0, 0));
                teller++;
            }
        }
        //voorkant kaart
        for (int i = 0; i < 7; i++) {
            this.gridpaneList.get(i).add(deck.getSpeelstapelKaarten().get(speelKaartnr).getKaartFoto(), i, 1);
            gridPaneSpel.setAlignment(Pos.TOP_CENTER);
            gridpaneList.get(i).setHalignment(deck.getSpeelstapelKaarten().get(speelKaartnr).getKaartFoto(), HPos.CENTER);
            gridpaneList.get(i).setValignment(deck.getSpeelstapelKaarten().get(speelKaartnr).getKaartFoto(), VPos.TOP);
            gridpaneList.get(i).setMargin(deck.getSpeelstapelKaarten().get(speelKaartnr).getKaartFoto(), new Insets(i * 30, 0, 0, 0));
            speelKaartnr++;
        }


    }


    int pushDeckTeller = 0;
    private int tellerDeckFoto = 0;

    public void deckPush() {
        for (Kaart deckKaart : deck.getDeck()) {
            if (pushDeckTeller == deck.deckSize()) {
                pushDeckTeller = 0;
                gridPaneDeck.getChildren().clear();
            }
            kaartFoto.add(deckKaart.getKaartFoto());
            gridPaneDeck.add(kaartFoto.get(tellerDeckFoto), 1, 0);
            pushDeckTeller++;
            tellerDeckFoto++;
        }
        gridPaneDeck.setAlignment(Pos.CENTER);
    }

    public List<GridPane> getGridpaneList() {
        return gridpaneList;
    }

    public void setTellerDeckFoto(int tellerDeckFoto) {
        this.tellerDeckFoto = tellerDeckFoto;
    }

    public int getTellerDeckFoto() {
        return tellerDeckFoto;
    }

    public List<ImageView> getKaartFoto() {

        return kaartFoto;
    }

    public Rectangle[] getStapels() {
        return stapels;
    }

    MenuItem getNewGameItem() {
        return newGame;
    }

    MenuItem getHighScoreItem() {
        return highScore;
    }

    MenuItem getExitItem() {
        return exit;
    }

    MenuItem getGameRules() {
        return gameRules;
    }

    MenuItem getAboutItem() {
        return about;
    }

    public Button getBtnDeck() {
        return btnDeck;
    }

    public GridPane getGridPaneSpel() {
        return gridPaneSpel;
    }

    public Rectangle[] getUitspelenRec() {
        return uitspelenRec;
    }

    public double getKaartBreedte() {
        return kaartBreedte;
    }

    public double getKaartHoogte() {
        return kaartHoogte;
    }

    public Deck getDeck() {
        return deck;
    }

    public void setSpeelKaartnr(int speelKaartnr) {
        this.speelKaartnr = speelKaartnr;
    }

    public boolean ligtNaarBoven(GridPane gridpane) {
        boolean ligtNaarBoven = false;
        for (Node image : gridpane.getChildren()) {
            if (image instanceof ImageView) {
                for (Kaart kaart : deck.getSpeelstapelKaarten()) {
                    if (kaart.getKaartFoto().getImage().equals(((ImageView) image).getImage())) {
                        ligtNaarBoven = true;
                    }
                }
            }
        }
        return ligtNaarBoven;
    }

    public int aantalImagesInGridPane(GridPane gridpane) {
        int aantalImageViews = 0;
        for (Node image : gridpane.getChildren()) {
            if (image instanceof ImageView) {
                aantalImageViews++;
            }
        }
        return aantalImageViews;
    }

    public Label getLblBottom() {
        return lblBottom;
    }

    public List<ImageView> getDeckImages() {
        return deckImages;
    }

    public int getSpeelKaartnr() {
        return speelKaartnr;
    }
}
