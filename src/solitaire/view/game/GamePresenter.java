package solitaire.view.game;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import solitaire.model.GameModel;
import solitaire.model.KaartCheck;
import solitaire.view.start.StartPresenter;

import java.io.BufferedReader;


public class GamePresenter {
    private GameModel model;
    //private Deck modelDeck;
    private KaartCheck kaartCheck = new KaartCheck();
    private GameView view;
    private Stage stage = new Stage();
    public Timeline stopwatchTimeline;
    private ImageView sourceDrag = new ImageView();
    private ImageView sourceDropImageView = new ImageView();
    private Rectangle sourceDropRectangle = new Rectangle();

    private int rijIndex = 0;
    private int kolomIndex = 0;
    private int rijIndexDrag = 0;
    private int kolomIndexDrag = 0;

    int aantalKinderenDrag1 = 1;
    int aantalKinderenDrag2 = 2;
    int aantalKinderenDrag3 = 3;
    int aantalKinderenDrag4 = 4;
    int aantalKinderenDrag5 = 5;
    int aantalKinderenDrag6 = 6;

    private int getNodeIndex;
    private int aantalKinderen;
    private int aantalKinderen1;
    private int aantalKinderen2;
    private int aantalKinderen3;
    private int aantalKinderen4;
    private int aantalKinderen5;
    private int aantalKinderen6;

    public GamePresenter(GameModel model, GameView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }


    private void addEventHandlers() {

        this.view.getNewGameItem().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                GameView gameView = new GameView();
                gameView.getStylesheets().add("css/main.css");
                GamePresenter gamePresenter = new GamePresenter(model, gameView);
                gamePresenter.stopwatchTimeline.play();
                view.getScene().setRoot(gameView);
            }
        });

        this.view.getHighScoreItem().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Label lblHighscore = new Label("Highscores");
                Group root = new Group(lblHighscore);
                Scene highScoreScene = new Scene(root, 600, 800);
                highScoreScene.setFill(Color.DARKGREEN);
                stage.setResizable(false);
                stage.setTitle("Highscores");
                stage.setScene(highScoreScene);
                stage.show();
            }
        });

        this.view.getExitItem().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Button btnExit = new Button("Exit");
                Label lblExit = new Label("Weet je zeker dat je het spel wil verlaten?");
                Group root = new Group(btnExit, lblExit);
                Scene exitScene = new Scene(root, 600, 300);
                exitScene.setFill(Color.DARKGREEN);
                stage.setResizable(false);
                stage.setTitle("Exit");
                stage.setScene(exitScene);
                stage.show();
            }
        });

        this.view.getGameRules().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Text txtGameRules = new Text("...");
                Group root = new Group(txtGameRules);
                Scene gameRulesScene = new Scene(root, 600, 800);
                gameRulesScene.setFill(Color.DARKGREEN);
                stage.setResizable(false);
                stage.setTitle("Game Rules");
                stage.setScene(gameRulesScene);
                stage.show();
            }
        });

        this.view.getAboutItem().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Text txtVersie = new Text("Solitaire versie 1.0");
                //txtVersie.setId("aboutTextOpmaak");
                Text txtDoor = new Text("Door");
                Text txtNamen = new Text("Axelle Storme en Laura Mutambay");
                txtVersie.setX(250);
                txtVersie.setY(130);
                txtDoor.setX(290);
                txtDoor.setY(150);
                txtNamen.setX(220);
                txtNamen.setY(170);
                /*txtVersie.setFont(Font.font("Actor", FontWeight.BOLD, 25));
                txtDoor.setFont(Font.font("Actor", FontWeight.BOLD, 25));
                txtNamen.setFont(Font.font("Actor", FontWeight.BOLD, 25));*/
                Group root = new Group(txtVersie, txtDoor, txtNamen);
                Scene aboutScene = new Scene(root, 600, 300);
                aboutScene.setFill(Color.DARKGREEN);
                stage.setResizable(false);
                stage.setTitle("About");
                stage.setScene(aboutScene);
                stage.show();

            }
        });
        EventHandler<MouseEvent> dragDetected = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                sourceDrag = (ImageView) event.getSource();
                sourceDrag.setFitHeight(view.getKaartHoogte());
                sourceDrag.setFitWidth(view.getKaartBreedte());
                Dragboard dragboard = sourceDrag.startDragAndDrop(TransferMode.MOVE);
                ClipboardContent content = new ClipboardContent();
                content.putImage(sourceDrag.getImage());
                dragboard.setContent(content);
                rijIndexDrag = GridPane.getRowIndex(sourceDrag);
                kolomIndexDrag = GridPane.getColumnIndex(sourceDrag);
                event.consume();
            }
        };
        EventHandler<DragEvent> onDragOver = new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                ImageView source = (ImageView) event.getSource();
                if (event.getGestureSource() != source && event.getDragboard().hasImage()) {
                    //als ik er nu oversleep dat die dat al ziet
                    event.acceptTransferModes(TransferMode.MOVE);
                }
                event.consume();
            }


        };

        EventHandler<DragEvent> onDragDropped = new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                Dragboard db = event.getDragboard();
                if (event.getSource() instanceof ImageView) {
                    sourceDropImageView = (ImageView) event.getSource();
                    kolomIndex = GridPane.getColumnIndex(sourceDropImageView);
                    if (rijIndex==0) {
                        if (kaartCheck.accepteerStapelKaart(view.getDeck().getKaart(sourceDrag), view.getDeck().getKaart(sourceDropImageView))) {

                            boolean succes = false;
                            if (db.hasImage()) {
                                succes = true;

                                //  modelSpeelStapel.imageKaartGedropt(db.getImage());
                                rijIndex = GridPane.getRowIndex(sourceDropImageView);
                                //kolomIndex = GridPane.getColumnIndex(sourceDropImageView);

                            }
                            event.setDropCompleted(succes);
                        } else {
                            event.consume();
                        }
                    }
                    if (kaartCheck.accepteerEindStapelKaart(view.getDeck().getKaart(sourceDrag),view.getDeck().getKaart(sourceDropImageView))) {
                        boolean succes = false;
                        if (db.hasImage()) {
                            succes = true;
                            rijIndex = GridPane.getRowIndex(sourceDropImageView);
                            //kolomIndex = GridPane.getColumnIndex(sourceDropImageView);
                        }
                        event.setDropCompleted(succes);
                    } else {
                        event.consume();
                    }
                } else {
                    if (kaartCheck.accepteerAas(view.getDeck().getKaart(sourceDrag))) {
                        sourceDropRectangle = (Rectangle) event.getSource();

                        boolean succes = false;
                        if (db.hasImage()) {
                            succes = true;

                            //  modelSpeelStapel.imageKaartGedropt(db.getImage());
                            rijIndex = GridPane.getRowIndex(sourceDropRectangle);
                            kolomIndex = GridPane.getColumnIndex(sourceDropRectangle);
                        }
                        event.setDropCompleted(succes);
                    } else {
                        event.consume();

                    }
                }
            }
        };
        //Dragdone rijIndex => van de vier eindstapels
        EventHandler<DragEvent> dragDone = new EventHandler<DragEvent>() {
            @Override
            public void handle(DragEvent event) {
                if (event.getTransferMode() == TransferMode.MOVE) {
//                    if (kaartCheck.accepteerStapelKaart(view.getDeck().getKaart(sourceDrag), view.getDeck().getKaart(sourceDropImageView))) {
                    if (rijIndex == 0) {
                        view.getGridPaneSpel().add(sourceDrag, kolomIndex, 0);
                        view.getGridPaneSpel().setMargin(sourceDrag, new Insets(0));
                        view.getGridPaneSpel().setHalignment(sourceDrag, HPos.CENTER);
                        view.getGridPaneSpel().setValignment(sourceDrag, VPos.CENTER);
                        switch (kolomIndex) {
                            case 3:
                                kaartCheck.getEindLijst1().add(0,sourceDrag.getImage());
                                break;
                            case 4:
                                kaartCheck.getEindLijst2().add(0,sourceDrag.getImage());
                                break;
                            case 5:
                                kaartCheck.getEindLijst3().add(0,sourceDrag.getImage());
                                break;
                            case 6:
                                kaartCheck.getEindLijst4().add(0,sourceDrag.getImage());
                                break;
                        }

                    } else {
                        aantalKinderen = view.aantalImagesInGridPane(view.getGridpaneList().get(kolomIndex));
                        view.getGridpaneList().get(kolomIndex).setMargin(sourceDrag, new Insets(aantalKinderen * 30, 0, 0, 0));
                        view.getGridpaneList().get(kolomIndex).add(sourceDrag, kolomIndex, 1);
                        aantalKinderen++;
                    }

                    // view.getDeck().veranderLijst(sourceDrag, sourceDropImageView);
                   // view.setTellerDeckFoto(view.getTellerDeckFoto());
                   /* } else {
                        event.consume();
                    }*/
                }
            }
        };

        EventHandler<MouseEvent> deckSpeelkaartKlik = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                ImageView source = (ImageView) event.getSource();
                //als de source foto niet gelijk is aan de vorige foto van uw speelkaartenstapel
                // dan kan je klikken om de kaart om te draaien
                if (!(source.getImage().equals(view.getDeck().getSpeelstapelKaarten().get(view.getSpeelKaartnr() - 1).getKaartFoto().getImage()))
                        && !view.ligtNaarBoven(view.getGridpaneList().get(GridPane.getColumnIndex(source)))) {
                    source.setImage(view.getDeck().getSpeelstapelKaarten().get(view.getSpeelKaartnr()).getKaartFoto().getImage());

                    view.setSpeelKaartnr(view.getSpeelKaartnr() + 1);
                    source.setOnDragDetected(dragDetected);
                    source.setOnDragOver(onDragOver);
                    source.setOnDragDone(dragDone);
                    source.setOnDragDropped(onDragDropped);
                }
                event.consume();
            }
        };

        for (
                int i = 0; i < view.getDeckImages().

                size();

                i++) {
            view.getDeckImages().get(i).setOnMouseClicked(deckSpeelkaartKlik);
        }


        Rectangle[] target = new Rectangle[4];
        for (
                int i = 0;
                i < 4; i++) {
            target[i] = view.getUitspelenRec()[i];
            target[i].setOnDragDetected(dragDetected);
            target[i].setOnDragOver(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    if (event.getGestureSource() != target && event.getDragboard().hasImage()) {
                        //als ik er nu oversleep dat die dat al ziet
                        event.acceptTransferModes(TransferMode.MOVE);
                    }
                    event.consume();
                }
            });
            target[i].setOnDragDropped(onDragDropped);
            target[i].setOnDragDone(dragDone);

        }


        for (
                int i = 0;
                i < 28; i++) {
            view.getDeck().getSpeelstapelKaarten().get(i).getKaartFoto().setOnDragDetected(dragDetected);
            view.getDeck().getSpeelstapelKaarten().get(i).getKaartFoto().setOnDragOver(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    if (event.getGestureSource() != view.getDeck().getSpeelstapelKaarten() && event.getDragboard().hasImage()) {
                        //als ik er nu oversleep dat die dat al ziet
                        event.acceptTransferModes(TransferMode.MOVE);

                    }
                    event.consume();
                }
            });
            view.getDeck().getSpeelstapelKaarten().get(i).getKaartFoto().setOnDragDropped(onDragDropped);
            view.getDeck().getSpeelstapelKaarten().get(i).getKaartFoto().setOnDragDone(dragDone);
        }
        for (
                int i = 0;
                i < 7; i++) {
            view.getStapels()[i].setOnDragDetected(dragDetected);
            view.getStapels()[i].setOnDragOver(new EventHandler<DragEvent>() {
                @Override
                public void handle(DragEvent event) {
                    if (event.getGestureSource() != view.getDeck().getSpeelstapelKaarten() && event.getDragboard().hasImage()) {
                        //als ik er nu oversleep dat die dat al ziet
                        event.acceptTransferModes(TransferMode.MOVE);

                    }
                    event.consume();
                }
            });
            view.getStapels()[i].setOnDragDropped(onDragDropped);
            view.getStapels()[i].setOnDragDone(dragDone);
        }

        //KLIK OP DE DECK
        this.view.getBtnDeck().

                setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        view.deckPush();

                        for (ImageView kaart : view.getKaartFoto()) {
                            kaart.setOnDragDetected(dragDetected);
                            kaart.setOnDragOver(onDragOver);
                            kaart.setOnDragDropped(onDragDropped);
                            kaart.setOnDragDone(dragDone);
                        }

                    }
                });
        this.stopwatchTimeline = new

                Timeline(new KeyFrame(Duration.millis(model.getTickDurationMillis()),
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        model.tick();
                        updateView();
                    }
                }));
        stopwatchTimeline.setCycleCount(Animation.INDEFINITE);

    }


    private void updateView() {
        this.view.getLblBottom().setText(String.format("Tijd: %02d:%02d:%02d", this.model.getHours(), this.model.getMinutes(), this.model.getSeconds()));
    }

    public void setSourceDrop(ImageView sourceDrop) {
        this.sourceDropImageView = sourceDropImageView;
    }


}

