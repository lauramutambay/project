package solitaire.view.start;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Font;


import java.util.Optional;

import static javafx.geometry.Pos.CENTER;
import static javafx.geometry.Pos.TOP_CENTER;

public class StartView extends BorderPane {
    private GridPane gridPaneStart;
    private Label lblSolitaire;
    private Label lblUsername;
    private TextField gebruikersnaamInput;
    private Button startBtn;
    private ImageView solitaireFoto;

    public StartView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        this.solitaireFoto=new ImageView("images/start.gif");
        this.gridPaneStart = new GridPane();
        this.lblSolitaire = new Label("Solitaire");
        //BorderPane.setAlignment(lblSolitaire, CENTER);
        this.lblUsername = new Label(("Username"));
        //BorderPane.setAlignment(lblUsername,TOP_CENTER);
        this.gebruikersnaamInput = new TextField("Gebruikersnaam");
        //BorderPane.setAlignment(txtfUsername,CENTER);
        this.startBtn = new Button("START");
        BorderPane.setMargin(startBtn, new Insets(30));
        this.setId("borderpaneStart");
    }

    private void layoutNodes() {
        setTop(lblSolitaire);
        lblSolitaire.setFont(new Font(100));
        setCenter(this.gridPaneStart);
        lblUsername.setFont(new Font(28));
        gebruikersnaamInput.setFont(new Font(28));
        gebruikersnaamInput.setMaxWidth(450);
        startBtn.setFont(new Font(28));

        RowConstraints row1 = new RowConstraints(250);
        RowConstraints row2 = new RowConstraints(250);
        RowConstraints row3 = new RowConstraints(250);
        RowConstraints row4 = new RowConstraints(250);
        gridPaneStart.getRowConstraints().addAll(row1,row2,row3,row4);
        gridPaneStart.add(lblUsername,0,2);
        gridPaneStart.add(gebruikersnaamInput,0,2);
        gridPaneStart.add(startBtn,0,3);
        gridPaneStart.setHalignment(lblUsername, HPos.LEFT);
        gridPaneStart.setValignment(lblUsername, VPos.CENTER);
        gridPaneStart.setHalignment(gebruikersnaamInput, HPos.RIGHT);
        gridPaneStart.setValignment(gebruikersnaamInput, VPos.CENTER);
        gridPaneStart.setValignment(startBtn, VPos.TOP);
        gridPaneStart.setHalignment(startBtn, HPos.CENTER);


        solitaireFoto.setFitWidth(750);
        solitaireFoto.setFitHeight(550);
        gridPaneStart.add(solitaireFoto,0,0);
        gridPaneStart.setValignment(solitaireFoto, VPos.BASELINE);
        gridPaneStart.setHalignment(solitaireFoto, HPos.CENTER);
        gridPaneStart.setAlignment(CENTER);



    }
    public Button getStartBtn() {
        return startBtn;

    }

    public TextField getGebruikersnaamInput() {
        return gebruikersnaamInput;
    }
}
