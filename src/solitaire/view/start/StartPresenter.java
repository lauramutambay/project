package solitaire.view.start;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import solitaire.model.GameModel;
import solitaire.model.Gebruiker;
import solitaire.model.GebruikerException;
import solitaire.view.game.GamePresenter;
import solitaire.view.game.GameView;

import java.io.IOException;


public class StartPresenter {
    private GameModel model;
    private StartView startView;

    public StartPresenter(GameModel model, StartView startView) {
        this.model = model;
        this.startView = startView;
        addEventHandler();
        updateView();

    }

    private void updateView() {
    }

    public void addEventHandler() {


        this.startView.getStartBtn().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

                String gebruikersnaam = startView.getGebruikersnaamInput().getText();
                try {
                    model.send(gebruikersnaam);
                } catch (GebruikerException e) {
                    //
                }

                GameView gameView = new GameView();
                gameView.getStylesheets().add("css/main.css");
                GamePresenter gamePresenter = new GamePresenter(model, gameView);
                gamePresenter.stopwatchTimeline.play();
                startView.getScene().setRoot(gameView);
            }

        });

    }

}