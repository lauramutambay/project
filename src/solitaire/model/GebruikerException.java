package solitaire.model;

import java.io.IOException;

public class GebruikerException extends IOException {
    public GebruikerException(String message){
        super(message);
    }

    public GebruikerException(Throwable cause) {
        super(cause);
    }
}
