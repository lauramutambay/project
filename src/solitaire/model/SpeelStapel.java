package solitaire.model;

import javafx.scene.image.ImageView;
import java.util.ArrayList;
import java.util.List;

public class SpeelStapel {
    private ImageView geselecteerdeKaart;
    private ImageView kaartGedropt;
    private int indexGeselecteerdeKaart=0;
    private int indexGedropteKaart=0;
    public SpeelStapel() {
    }


    public List<Kaart> maakStapel(int aantalKaarten, Deck deck){
        List<Kaart> lijst= new ArrayList<>();
        for (int i = 0; i < aantalKaarten; i++) {
            lijst.add(deck.getSpeelstapelKaarten().get(0));
            deck.verwijderSpeelstapelKaart(deck.getSpeelstapelKaarten().get(0));
            //deck.getSpeelstapelKaarten().remove(0);
        }
        return lijst;
    }

}
