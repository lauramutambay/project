package solitaire.model;

import javafx.scene.image.ImageView;

public class Kaart {
    public enum KaartSoort{
        HARTEN,SCHOPPEN,RUITEN,KLAVEREN;

        public String toString() {
            return name().toLowerCase();
        }
    }
    public enum KaartKleur{
        ZWART,ROOD;

        @Override
        public String toString() {
            return name().toLowerCase();
        }
    }
    public enum KaartWaarden {
        AAS(1), TWEE(2), DRIE(3), VIER(4), VIJF(5), ZES(6),
        ZEVEN(7), ACHT(8), NEGEN(9), TIEN(10), BOER(11), VROUW(12), HEER(13);

        private int kaartWaarden;

        KaartWaarden(int deckKaarten) {
            this.kaartWaarden = deckKaarten;
        }
        public int getKaartWaarden() {
            return kaartWaarden;
        }

        @Override
        public String toString() {
            return name().toLowerCase();
        }
    }

    private KaartSoort kaartSoort;
    private KaartKleur kaartKleur;
    private KaartWaarden kaartWaarden;
    private ImageView kaartFoto;

    @Override
    public String toString() {
        return "Kaart{" +
                "kaartSoort=" + kaartSoort +
                ", kaartKleur=" + kaartKleur +
                ", kaartWaarden=" + kaartWaarden +
                ", kaartFoto=" + kaartFoto +
                '}';
    }

    public Kaart() {
    }

    public Kaart(KaartSoort kaartSoort, KaartKleur kaartKleur, KaartWaarden kaartWaarden, ImageView kaartFoto) {
        this.kaartSoort = kaartSoort;
        this.kaartKleur = kaartKleur;
        this.kaartWaarden = kaartWaarden;
        this.kaartFoto = kaartFoto;
    }

    public KaartSoort getKaartSoort() {
        return kaartSoort;
    }

    public KaartKleur getKaartKleur() {
        return kaartKleur;
    }

    public KaartWaarden getKaartWaarden() {
        return kaartWaarden;
    }

    public ImageView getKaartFoto() {
        return kaartFoto;
    }



}
