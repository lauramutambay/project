package solitaire.model;

import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Rectangle;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Integer.parseInt;

public class KaartCheck extends Deck{
    public List<Image> eindLijst1;
    public List<Image> eindLijst2;
    public List<Image> eindLijst3;
    public List<Image> eindLijst4;

    public KaartCheck() {
        this.eindLijst1 = new ArrayList<>();
        this.eindLijst2 = new ArrayList<>();
        this.eindLijst3 = new ArrayList<>();
        this.eindLijst4 = new ArrayList<>();
    }

    public List<Image> getEindLijst1() {
        return eindLijst1;
    }

    public List<Image> getEindLijst2() {
        return eindLijst2;
    }

    public List<Image> getEindLijst3() {
        return eindLijst3;
    }

    public List<Image> getEindLijst4() {
        return eindLijst4;
    }

    //Methode voor wanneer een kaart op een van 7 voorlopige stapels geplaatst wordt
    public boolean accepteerStapelKaart(Kaart verlegdeKaart, Kaart liggendeKaart) {
        if ((!(verlegdeKaart.getKaartKleur().equals(liggendeKaart.getKaartKleur())))
                && (verlegdeKaart.getKaartWaarden().ordinal()==(liggendeKaart.getKaartWaarden().ordinal()-1))) {
            return true; //kaart mag geplaatst worden
        }
        return false; //kaart mag niet geplaatst worden
    }
    public boolean accepteerEindStapelKaart(Kaart verlegdeKaart, Kaart liggendeKaart) {
        if (verlegdeKaart.getKaartKleur().equals(liggendeKaart.getKaartKleur())
                && (verlegdeKaart.getKaartWaarden().ordinal()==(liggendeKaart.getKaartWaarden().ordinal() + 1))) {
            return true;
        }
        return false;
    }
    public boolean accepteerAas(Kaart verlegdeKaart){
        if (verlegdeKaart.getKaartWaarden().name().toString().equals("AAS")){
            return true;
        }
        return false;
    }

    //Als de eindstapel gelijk is aan één van de stapels, dan true
    public boolean eindStapelCheck(ArrayList eindLijst) {
        Collections.sort(eindLijst, new LijstComparator());
        if (eindLijst.equals(getStapelHarten()) || eindLijst.equals(getStapelKlaveren()) || eindLijst.equals(getStapelRuiten()) || eindLijst.equals(getStapelSchoppen()) ) {
            return true;
        }
        return false;
    }

    public boolean gewonnen(Boolean gewonnen) {
        if (eindLijst1.equals(gewonnen) && eindLijst2.equals(gewonnen) && eindLijst3.equals(gewonnen) && eindLijst4.equals(gewonnen)) {
            return true; //dan heb je het spel gewonnen
        }
        return false;
    }

}
