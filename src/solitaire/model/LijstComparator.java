package solitaire.model;

import javafx.scene.image.Image;

import java.util.Comparator;

public class LijstComparator implements Comparator<Kaart> {
    @Override
    public int compare(Kaart o1, Kaart o2) {
        return o2.getKaartWaarden().ordinal() - o1.getKaartWaarden().ordinal() ;
    }
}
