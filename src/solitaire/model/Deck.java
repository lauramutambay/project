package solitaire.model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.*;

public class Deck extends Kaart {
    private List<Kaart> deckKaarten;
    private int volgnrDeckKaart = 0;
    private Kaart[] stapelSchoppen = new Kaart[13];
    private Kaart[] stapelHarten = new Kaart[13];
    private Kaart[] stapelKlaveren = new Kaart[13];
    private Kaart[] stapelRuiten = new Kaart[13];
    private List<Kaart> speelstapelKaarten;
    private double kaartBreedte = 143;
    private double kaartHoogte = 219;
    private Boolean isDeckImage=false;
    public Deck() {
        this.deckKaarten = new ArrayList<>();
        this.speelstapelKaarten = new ArrayList<>();
        vulStapelsPerSoort();
        schudden();
        vulSpeelStapel();
    }

    private void vulSpeelStapel() {
        for (int i = 0; i < 28; i++) {
            speelstapelKaarten.add(getDeckKaarten().get(0));

            getDeckKaarten().remove(0);
        }
    }

    public void verwijderSpeelstapelKaart(Kaart kaart) {
        for (Iterator<Kaart> iterator = speelstapelKaarten.iterator(); iterator.hasNext(); ) {
            Kaart next = iterator.next();
            if (kaart.getKaartFoto().equals(next.getKaartFoto())) {
                iterator.remove();
            }
        }
    }

    public List<Kaart> getSpeelstapelKaarten() {
        return speelstapelKaarten;
    }

    public List<Kaart> getDeckKaarten() {
        return deckKaarten;
    }

    public void setDeckKaarten(List<Kaart> deckKaarten) {
        this.deckKaarten = deckKaarten;
    }

    public Kaart[] getStapelSchoppen() {
        return stapelSchoppen;
    }

    public Kaart[] getStapelHarten() {
        return stapelHarten;
    }

    public Kaart[] getStapelKlaveren() {
        return stapelKlaveren;
    }

    public Kaart[] getStapelRuiten() {
        return stapelRuiten;
    }

    public Kaart getDeckKaarten(int index) {
        return deckKaarten.get(index);
    }

    public void removeDeckKaart(Kaart kaart) {
        for (Iterator<Kaart> iterator = deckKaarten.iterator(); iterator.hasNext(); ) {
            Kaart next = iterator.next();
            if (next.equals(kaart)) {
                iterator.remove();

            }
        }
    }

    public Kaart getKaart(ImageView imageView) {
        Kaart kaartObject = new Kaart();
        for (Kaart kaart : deckKaarten) {
            if (kaart.getKaartFoto().getImage().equals(imageView.getImage())) {
                kaartObject = kaart;
            }
        }
        for (Kaart kaart : speelstapelKaarten) {
            if (kaart.getKaartFoto().getImage().equals(imageView.getImage())) {
                kaartObject = kaart;
            }
        }
        return kaartObject;
    }

    public void veranderLijst(ImageView drag, ImageView dropped) {
        for (Kaart kaart : deckKaarten) {
            if (kaart.getKaartFoto().equals(drag)) {
                veranderDeckLijst(drag,dropped);
            }
        }
        for (Kaart kaart : speelstapelKaarten) {
            if (kaart.getKaartFoto().equals(drag)) {
                veranderSpeelStapelKaarten(getKaart(drag), getKaart(dropped));
            }
        }
    }

    public void veranderDeckLijst(ImageView drag,ImageView drop) {
        List<Kaart> nieuweDeckKaarten = new ArrayList<>();
        List<Kaart> nieuweSpeelstapelKaarten = new ArrayList<>();
        for (int i = 0; i < speelstapelKaarten.size(); i++) {
            nieuweSpeelstapelKaarten.add(speelstapelKaarten.get(i));
        }
        for (int i = 0; i < deckKaarten.size(); i++) {
            nieuweDeckKaarten.add(deckKaarten.get(i));
        }
        for (int i = 0; i < nieuweDeckKaarten.size(); i++) {
            if (nieuweDeckKaarten.get(i).getKaartFoto().equals(drag)) {
                nieuweDeckKaarten.remove(i);
            }
        }
        for (int i = 0; i < nieuweSpeelstapelKaarten.size(); i++) {
            if (nieuweSpeelstapelKaarten.get(i).getKaartFoto().equals(drop)) {
                nieuweSpeelstapelKaarten.add(i + 1, getKaart(drag));
            }
        }
        setDeckKaarten(nieuweDeckKaarten);
        setSpeelstapelKaarten(nieuweSpeelstapelKaarten);
    }


    public void veranderSpeelStapelKaarten(Kaart drag, Kaart drop) {
        List<Kaart> nieuweSpeelstapelKaarten = new ArrayList<>();

        int indexDragImg = 0;
        for (int i = 0; i < speelstapelKaarten.size(); i++) {
            nieuweSpeelstapelKaarten.add(speelstapelKaarten.get(i));
        }
        for (int i = 0; i < nieuweSpeelstapelKaarten.size(); i++) {
            if (nieuweSpeelstapelKaarten.get(i).getKaartFoto().equals(drag.getKaartFoto())) {
                indexDragImg = i;
                nieuweSpeelstapelKaarten.remove(i);
            }
        }
        for (int i = 0; i < nieuweSpeelstapelKaarten.size(); i++) {
            if (nieuweSpeelstapelKaarten.get(i).getKaartFoto().equals(drop.getKaartFoto())) {
                 nieuweSpeelstapelKaarten.add(i + 1, drag);
            }
        }
        setSpeelstapelKaarten(nieuweSpeelstapelKaarten);
    }

    public void setSpeelstapelKaarten(List<Kaart> speelstapelKaarten) {
        this.speelstapelKaarten = speelstapelKaarten;
    }

    private void vulStapelsPerSoort() {
        int teller = 1;
        for (KaartWaarden kaartWaarden : KaartWaarden.values()) {
            this.stapelSchoppen[teller - 1] = new Kaart(Kaart.KaartSoort.SCHOPPEN, Kaart.KaartKleur.ZWART, kaartWaarden, new ImageView("images/" + teller + "S.png"));
            this.stapelHarten[teller - 1] = new Kaart(Kaart.KaartSoort.HARTEN, Kaart.KaartKleur.ROOD, kaartWaarden, new ImageView("images/" + teller + "H.png"));
            this.stapelKlaveren[teller - 1] = new Kaart(Kaart.KaartSoort.KLAVEREN, Kaart.KaartKleur.ZWART, kaartWaarden, new ImageView("images/" + teller + "C.png"));
            this.stapelRuiten[teller - 1] = new Kaart(Kaart.KaartSoort.RUITEN, Kaart.KaartKleur.ROOD, kaartWaarden, new ImageView("images/" + teller + "D.png"));
            teller++;

        }
    }

    private void schudden() {
        for (int i = 0; i < 13; i++) {
            this.deckKaarten.add(stapelSchoppen[i]);
            this.deckKaarten.add(stapelHarten[i]);
            this.deckKaarten.add(stapelKlaveren[i]);
            this.deckKaarten.add(stapelRuiten[i]);
        }
        Collections.shuffle(deckKaarten);
        for (int i = 0; i < 52; i++) {
            deckKaarten.get(i).getKaartFoto().setFitHeight(kaartHoogte);
            deckKaarten.get(i).getKaartFoto().setFitWidth(kaartBreedte);
        }


    }
    public boolean isDeckImageKaart(ImageView deckKaart){
        if(deckKaart.equals(new ImageView(new Image("images/gray_back.png")))){
            isDeckImage=true;
        }
        return isDeckImage;
    }
    public ImageView getVorigeKaart(ImageView dragKaart){
        int indexVorigeKaart = 0;
        for (int i = 0; i < speelstapelKaarten.size(); i++) {
            if (speelstapelKaarten.get(i).getKaartFoto().equals(dragKaart)){
                indexVorigeKaart=i-1;
            }
        }
        return speelstapelKaarten.get(indexVorigeKaart).getKaartFoto();
    }
    private Kaart getKaart(int index) {
        return deckKaarten.get(index);
    }
    public Kaart getKaart() {
        return deckKaarten.get(volgnrDeckKaart);
    }

    //DE SET WORDT GEBRUIKT OM KAARTEN TE VERWIJDEREN UIT DE DECK
    //ALS ER EEN KAART WEGGENOMEN WORDT
    public void setDeckKaarten(ImageView kaartImg) {
        for (Iterator<Kaart> iterator = deckKaarten.iterator(); iterator.hasNext(); ) {
            Kaart next = iterator.next();
            if (next.getKaartFoto().getImage() == kaartImg.getImage()) {
                iterator.remove();
            }
        }
    }

    public int deckSize() {
        return deckKaarten.size();
    }

    public List<Kaart> getDeck() {
        List<Kaart> kaartList = new ArrayList<>();
        //de conditie van de if moet nog beter uitgewerkt worden zodanig dat deze nog steeds werkt als er een kaart wordt weggenomen
        if (volgnrDeckKaart == (deckKaarten.size() - deckKaarten.size() % 3)) {
            for (int i = 0; i < deckKaarten.size() % 3; i++) {
                kaartList.add(getKaart(volgnrDeckKaart));
                volgnrDeckKaart++;
            }
        } else {

            for (int i = 0; i < 3; i++) {
                kaartList.add(getKaart(volgnrDeckKaart));
                volgnrDeckKaart++;

            }
        }
        if (volgnrDeckKaart == deckKaarten.size()) {
            volgnrDeckKaart = 0;
        }
        return kaartList;
    }
}
