package solitaire.model;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

public class Gebruiker {
    private List<String> usersList;
    private String gebruikersnaam;
    private int score;
    private TreeMap<Integer,List<String>> highscores = new TreeMap<>(Collections.reverseOrder());

    public Gebruiker() {
        this.gebruikersnaam = "";
        this.score = 0;
    }

    private void nieuweGebruiker(String gebruikersnaam) throws GebruikerException{
        try (FileWriter fileWriter = new FileWriter("resources/tekstBestanden/users.txt",true)){
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(gebruikersnaam+";");
            bufferedWriter.newLine();
            bufferedWriter.close();
        } catch (IOException e) {
            throw new GebruikerException(e);
        }
    }

    private void veranderScore(int score) throws GebruikerException{
        try(FileWriter fileWriter = new FileWriter("resources/tekstBestanden/users.txt",true)){
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(String.valueOf(score) + '\n');
        } catch (IOException e) {
            throw new GebruikerException(e);
        }
    }

    /*private void verzamelHighscores() throws GebruikerException {
        try (BufferedReader reader = new BufferedReader(new FileReader("resources/tekstBestanden/users.txt"))){
            String highscore;
            while (!((highscore=reader.readLine())==null)) {
                String[] userScore = highscore.split(";");
                Integer score = Integer.valueOf(userScore[1]);
                if ((usersList= usersList.get(score))==null){
                    usersList = new ArrayList<>();
                    usersList.add(userScore[0]);
                    highscores.put(Integer.valueOf(userScore[1],usersList));
                } else {
                    usersList.add(userScore[0]);
                }
            }
        } catch(IOException e){
            throw new GebruikerException(e);
        }
    }*/

    public void send(String gebruikersnaam) throws GebruikerException {
        nieuweGebruiker(gebruikersnaam);

    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score=score;
    }

    public TreeMap<Integer, List<String>> getHighscores(){
        return highscores;
    }

}
