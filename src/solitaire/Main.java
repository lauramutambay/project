package solitaire;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import solitaire.model.Deck;
import solitaire.model.GameModel;
import solitaire.model.SpeelStapel;
import solitaire.view.start.StartPresenter;
import solitaire.view.start.StartView;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        GameModel model = new GameModel();
        StartView view = new StartView();
        StartPresenter presenter = new StartPresenter(model, view);
        view.getStylesheets().add("css/main.css");
        primaryStage.setScene(new Scene(view));
        primaryStage.setTitle("Solitaire");
        primaryStage.setHeight(1000);
        primaryStage.setWidth(1400);
        primaryStage.setResizable(false);
        primaryStage.show();

    }
    public static void main(String[] args) {
        Application.launch(args);

    }

   /* de dragOver is mij gelukt (op een rare manier maar het is me gelukt)
        het enige wat er nog niet perfect gaat is als je een kaart wegneemt van de deck
    dat je de volgende kaart niet kan draggen dus daar moeten we nog een oplossing voor vinden

    ook moet er een klasse worden gemaakt (dus een nieuw model) om de juiste kaarten te accepteren in
    de dragDone (dus op de 4 Rectangles ("uitlegstapels")) en moet de imageView van de kaart daar komen te staan
    (ik dacht iets met: als je de kaart verwijdert dmv de methode setDeckKaarten in de klasse deck
    dat de Rectangles er van op de hoogte worden gebracht en eerst ziet of deze matched met een AAS in het begin enzovoort)

    wat er ook nog niet gaat:
    als er een kaart wordt uitgelegd dan wordt deze verwijdert door de methode setDeckKaarten
            deze methode kan wel de juiste kaart selecteren om te verwijderen uit de lijst
            maar eens dat deze verwijdert wordt en je de deck terug gaat beginnen (dus terug vanaf index 0 van de lijst)
        dan komt er geen juiste volgende kaart maar neemt hij gewoon een nieuwe kaart die niet in de deck stond maar in
            uw speelkaarten staan*/
}
